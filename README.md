# 1. Create a new project
Go to [CERN PaaS](https://paas.cern.ch) and create a new project.

# 2. Fork repositories
Fork repositories from the [ALICE MasterClass](https://gitlab.cern.ch/alice-masterclass) group on GitLab.

# 3. PostgreSQL Database
Go back to CERN PaaS, then from the `Developer` view, select `Add > Database > PostgreSQL`.

It is important to choose the standard, non-ephemeral version.

Set the username, password and the database name (or leave them blank for auto-generation). Set the service name to `masterclass-database`.

# 4. Create `django-config` secret
From the `Administrator` view, select `Workloads > Secrets > Create > Key/value secret`. Name the secret "django-config". Now, create the following set of keys (in brackets I provided values from my configuration, for reference):

- `ALLOWED_HOSTS`: Django backend's URL. (`api-alice-masterclass.app.cern.ch`)
- `CORS_ALLOWED_ORIGINS`: URL's of both frontend websites separated by comma (no spaces!). (`https://alice-masterclass.app.cern.ch,https://teacher-alice-masterclass.app.cern.ch`)
- `DJANGO_SECRET_KEY`: random key used by Django for security, can be set manually or can be generated by an online tool, for example [here](https://djecrety.ir/).
- `FRONTEND_URL`: Where Django backend will redirect after succesful CERN SSO login. (`https://teacher-alice-masterclass.app.cern.ch/login`)
- `REDIRECT_URI`: URL of Django backend that handles CERN SSO. (`https://api-alice-masterclass.app.cern.ch/oauth`)
- `SERVICE_HOST`: Service name of the PostgreSQL database. (`alice-masterclass-sql`)
- `SERVICE_PORT`: Port of the PostreSQL database. (`5432`).

# 5. Create Image Import Token
To obtain the `IMAGE_IMPORT_TOKEN`, follow `Redeploy method 3: use a GitLab CI pipeline step to redeploy your application` section of [this](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004574) tutorial. 

Download the `oc` tool from https://paas.cern.ch/command-line-tools. In the CERN PaaS, click on your name in the upper right corner
of the website and select the `Copy login command option`. Use the provided command to login to PaaS, then follow the tutorial.

Keep in mind that the connection with the PaaS server using the `oc` tool can only be made from inside the CERN internal network. If you are doing the configuration from outside CERN, you can perform the required operations on a LXPlus server.

Keep your token for the next step.

# 6. Create GitLab variables
On each forked repository on GitLab, go to (`Settings > CI/CD > Variables`), then add a new variable `IMAGE_IMPORT_TOKEN`. Set its value to the token obtained from the previous step.
Repeat for the variable `NAMESPACE` and set it to the project's name on CERN PaaS.
Repeat for the variable `API_URL` and set it to the API address (`https://api-alice-masterclass.app.cern.ch/api/v1/`).

Additionally, on the forked `alice-masterclass-teacher` repository add the following variables:

- `CLIENT_ID`: Go to CERN PaaS, `Developer` view, `Secrets > oidc-client-secret`, then look at the value of `clientID`. It should be `webframeworks-paas-<name-of-your-paas-project>`. This is what you should put here.
- `REDIRECT_URI`: Set to the same URL as configured in step 4,
- `MASTERCLASS_HOST`: Address of the student website (`https://alice-masterclass.app.cern.ch/`),
- `OPENID_CONFIG_URL`: CERN OAuth config URL (`https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration`)

# 7. Generate Container Registries
Go to each forked repository on GitLab and create a new Tag (`Repository > Tags`).

This will trigger the GitLab CI and everything will start to build. If all goes well, pipeline in every project ( `CI/CD > Pipelines` )
should fail on the last step (redeployment). This is normal, as we didn't configure the OpenShift side of things yet.

# 8. Create Django backend deployment
Go to the forked `alice-masterclass-django` project on GitLab, `Packages & Registries > Container Registry` and click on the clipboard button on the right to the `Root image` text.

Go to CERN PaaS, `Developer` view, `Add > Container Image`, paste (Ctrl+V) the URL in the `Image name from external registry` field, select the desired runtime icon, application name and component name, select the `Resource` type as `DeploymentConfig` (important!), click on the `Routing` link in the `Advanced options` to access the routing settings, put `api-alice-masterclass.app.cern.ch` as `Routing > Hostname`, tick the `Security > Secure Route`, select `Security > TLS termination` to `Edge` and `Security > Insecure traffic` to `Redirect`. Click `Create`. The application will not start properly due to missing configuration. This is normal.

## 8.1 Add environment variables
Go to `Topology`, click on the Django backed globule, select `Actions > Edit DeploymentConfig`. In the editor, under `template > spec > containers`, before `resources: {}` line, add the following fragment:
```yml
          env:
            - name: DATABASE_NAME
              valueFrom:
                secretKeyRef:
                  name: masterclass-database
                  key: database-name
            - name: DATABASE_USER
              valueFrom:
                secretKeyRef:
                  name: masterclass-database
                  key: database-user
            - name: DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: masterclass-database
                  key: database-password
            - name: SERVICE_HOST
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: SERVICE_HOST
            - name: SERVICE_PORT
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: SERVICE_PORT
            - name: DJANGO_SECRET_KEY
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: DJANGO_SECRET_KEY
            - name: ALLOWED_HOSTS
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: ALLOWED_HOSTS
            - name: CORS_ALLOWED_ORIGINS
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: CORS_ALLOWED_ORIGINS
            - name: REDIRECT_URI
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: REDIRECT_URI
            - name: FRONTEND_URL
              valueFrom:
                secretKeyRef:
                  name: django-config
                  key: FRONTEND_URL
            - name: CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: oidc-client-secret
                  key: clientID
            - name: CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: oidc-client-secret
                  key: clientSecret
```
Click save. OpenShift should redeploy the app, this time with success.

## 8.2 Populate the database
Select the `Developer` view, `Topology`, select the Django backend globule again, click on the `Resources`, then on the link listed under `Pod`, select `Terminal`, wait for it to connect, then type:
```bash
python manage.py makemigrations
```
```bash
python manage.py migrate
```
## 8.3 Make it public
Go back to `Topology`, select the Django backend globule again, `Resources > Routes > (select the only route here) > Actions > Edit annotations`, then clear the `VALUE` from `haproxy.router.openshift.io/ip_whitelist` (do NOT delete the whole key, just clear the value).

# 9. Create student app website
Go to the forked `alice-masterclass-js` project on GitLab, `Packages & Registries > Container Registry` and click on the clipboard button on the right to the `Root image` text.

Go to CERN PaaS, `Developer` view, `Add > Container Image`, paste (Ctrl+V) the URL in the `Image name from external registry` field, the select the desired runtime icon, application name and component name, select the `Resource` type as `DeploymentConfig` (important!), click on the `Routing` link in the `Advanced options` to access the routing settings, put `alice-masterclass.app.cern.ch` as `Routing > Hostname`, tick the `Secure Route`, select `TLS termination` to `Edge` and `Insecure traffic` to `Redirect`. Click `Create`.

## 9.1 Make it public
Go back to `Topology`, select the student app globule, `Resources > Routes > (select the only route here) > Actions > Edit annotations`, then clear the `VALUE` from `haproxy.router.openshift.io/ip_whitelist` (do NOT delete the whole key, just clear the value).

# 10. Create teacher app website
Go to the forked `alice-masterclass-teacher` project on GitLab, `Packages & Registries > Container Registry` and click on the clipboard button on the right to the `Root image` text.

Go to CERN PaaS, `Developer` view, `Add > Container Image`, paste (Ctrl+V) the URL in the `Image name from external registry` field, the select the desired runtime icon, application name and component name, select the `Resource` type as `DeploymentConfig` (important!), click on the `Routing` link in the `Advanced options` to access the routing settings, put `teacher-alice-masterclass.app.cern.ch` as `Routing > Hostname`, tick the `Secure Route`, select `TLS termination` to `Edge` and `Insecure traffic` to `Redirect`. Click `Create`.

## 10.1 Make it public
Go back to `Topology`, select the teacher app globule, `Resources > Routes > (select the only route here) > Actions > Edit annotations`, then clear the `VALUE` from `haproxy.router.openshift.io/ip_whitelist` (do NOT delete the whole key, just clear the value).

# 11. Configure OAuth2
Click the `Import YAML` button (the "+" button located in the upper right corner of the PaaS page) and paste in the editor the following:
```yml
apiVersion: webservices.cern.ch/v1alpha1
kind: OidcReturnURI
metadata:
  name: teacher-alice-masterclass-return-uri
spec:
  redirectURI: 'https://api-alice-masterclass.app.cern.ch/oauth'
```
# 11. Do the final redeployment
Go to each forked repository on GitLab and create a new Tag (`Repository > Tags`). You can recreate (delete and create again) the tag made in step 6. At this point the triggered pipeline on each repository should be successfully completed.

# Warning
At the moment there is an issue with the automatic build of Electron app for Windows that causes the resulting executable to sometimes be corrupted (the app can not be started at all or the Visual Analysis part doesn't work) for no apparent reason. I did not encounter this with the Linux build, which is made in the same exact way.

After the deployment is complete please check if the program offered on the website functions properly. If not, try to deploy the `alice-masterclass-js` once again to force a rebuild.
